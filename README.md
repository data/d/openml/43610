# OpenML dataset: Risk-of-being-drawn-into-online-sex-work-(cleaned)

https://www.openml.org/d/43610

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset is the resulting cleaned version of Panos Kostakos's Risk of being drawn into online sex work dataset.
Context
This database was used in the paper: "Covert online ethnography and machine learning for detecting individuals at risk of being drawn into online sex work". https://www.flinders.edu.au/centre-crime-policy-research/illicit-networks-workshop
Content
The database includes data scraped from a European online adult forum. Using covert online ethnography we interviewed a small number of participants and determined their risk to either supply or demand sex services through that forum. This is a great dataset for semi-supervised learning.
Acknowledgements
The dataset was initially publicized by Panos Kostakos.
Inspiration
How can we identify individuals at risk of being drawn into online sex work? The spread of online social media enables a greater number of people to be involved into online sex trade; however, detecting deviant behaviors online is limited by the low available of data. To overcome this challenge, we combine covert online ethnography with semi-supervised learning using data from a popular European adult forum.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43610) of an [OpenML dataset](https://www.openml.org/d/43610). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43610/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43610/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43610/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

